# Ingestão de Dados de Coleta Domiciliar no Observatório de Resíduos Sólidos (PoC)

Prefeitura de Fortaleza<br>
Instituto de Planejamento de Fortaleza - Iplanfor<br>
Diretoria do Observatório da Governança Municipal - Diobs

<b>Autor:</b> Pedro Florencio de Almeida Neto - Cientista de Dados
<br><b>Contato:</b> <i>pedro.almeida@iplanfor.fortaleza.ce.gov.br</i>

## 1. Contextualização

O [Observatório de Resíduos Sólidos]('https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/obsr/') é uma iniciativa do programa Mais Fortaleza, elaborado pela Prefeitura de Fortaleza e executado pelo Instituto de Planejamento de Fortaleza. Trata-se de um painel informativo que disponibiliza dados sobre os resíduos coletados, destacando os pontos de coleta domiciliar e os locais de entrega voluntária para materiais recicláveis separados. Este repositório abrange toda a estrutura de dados, desde a aquisição até os dados processados para visualização no painel.

## 2. Metodologia

#### 2.1. Arquitetura Delta Lake

Delta Lake é uma estrutura robusta e confiável para a construção de pipelines de dados eficientes e escaláveis (Figura 01). Nesta estrutura, os dados são armazenados por meio de camadas. Na camada *bronze*, os dados crus e não processados são armazenados. À medida que os dados são refinados e preparados para análises mais aprofundadas, a camada *silver* é implementada. Nesta fase, podem ser aplicadas transformações e limpezas nos dados sem comprometer sua integridade. A camada *gold*, contém dados prontos para análises avançadas e tomada de decisões.

**Figura 01:** Arquitetura Delta Lake
<img src="https://www.databricks.com/wp-content/uploads/2019/08/Delta-Lake-Multi-Hop-Architecture-Bronze.png" alt="Arquitetura Delta Lake">
Fonte: [Databricks](https://www.databricks.com/)

#### 2.2. Extração
##### 2.2.1. Coleta Domiciliar
Os dados de coleta domiciliar são extraídos do sistema ERP da Marquise Ambiental, por meio de um *web crawler* contido na pasta *scripts*. Utiliza-se a biblioteca Selenium na versão 4.15.2.

#### 2.3. Transformação
Em desenvolvimento.

#### 2.4. Carregamento
Em desenvolvimento.

## 3. Instalação
#### 3.1. Credenciais
Para solicitação do arquivo das credenciais de acesso aos sistemas, envie um email para observatoriodefortaleza@iplanfor.fortaleza.ce.gov.br.

#### 3.2. Dependências
As dependências do projeto estão presentes no arquivo [requirements.txt](https://gitlab.com/pedroflorencio/pipelinedados/-/blob/main/requirements.txt?ref_type=heads). Recomenda-se executá-las em um ambiente virtual.

## 4. Tecnologias 
<div>
    <img src="https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54"
    height="22px" />
    <img src="https://img.shields.io/badge/-selenium-%43B02A?style=for-the-badge&logo=selenium&logoColor=white" height="22px" />
    <img src="https://img.shields.io/badge/Prefect-%23ffffff.svg?style=for-the-badge&logo=prefect&logoColor=blue" height="22px" />
    <img src="https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white" height="22px" />

</div>
