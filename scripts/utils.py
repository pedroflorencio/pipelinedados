from prefect import get_run_logger

def log(message:str):
    logger = get_run_logger()
    logger.info(message)