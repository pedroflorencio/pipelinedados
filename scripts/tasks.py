import os
import psycopg2
import configparser
import pandas as pd
from utils import log
from time import sleep
from prefect import task
from selenium import webdriver
from datetime import datetime,timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

# acessando arquivo de variaveis de ambiente
parser = configparser.ConfigParser()
parser.read('pipeline.conf')

# ----
PATH_DOWNLOAD = os.getcwd()+'/raw data'
HOST = parser.get('postgres_credentials','host')
PORT = parser.get('postgres_credentials','port')
DBNAME = parser.get('postgres_credentials','database')
USERNAME_DB = parser.get('postgres_credentials','username')
PASSWORD_DB = parser.get('postgres_credentials','password')
URL_SISTEMA = parser.get('erp_marquise_credentials','url')
USERNAME_SISTEMA = parser.get('erp_marquise_credentials', 'username')
PASSWORD_SISTEMA = parser.get('erp_marquise_credentials', 'password')
# ----

@task(name='extract_coleta')
def extract_coleta() -> str:
    # -- Web Crawler para Acesso ao Sistema ERP da Marquise --

    # configurando o webdriver (Firefox)
    options = Options()

    log('Iniciando configuração do navegador')
    # selecionando o local em que os dados serao salvos
    options.set_preference("browser.download.folderList", 2)
    options.set_preference("browser.download.dir", PATH_DOWNLOAD)
    options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.ms-excel")
    log('Navegador configurado com sucesso!')

    # evitando que a janela de dialogo de download apareca
    options.set_preference("browser.download.manager.showWhenStarting", False)
    options.set_preference("pdfjs.disabled", True)

    # desativando a interface grafica
    options.add_argument("--headless")

    driver = webdriver.Firefox(options=options)
    
    # acessando url do sistema
    driver.get(URL_SISTEMA)
    log('URL acessada com sucesso!')

    # realizando login na pagina
    username = driver.find_element(By.ID, 'usernameField')
    password = driver.find_element(By.ID, "passwordField")
    username.send_keys(USERNAME_SISTEMA)
    password.send_keys(PASSWORD_SISTEMA)
    driver.find_element(By.XPATH, '//button[@class="OraButton left"]').click()
    sleep(10)
    log('Login efetuado com sucesso!')

    # selecionando a secao de relatorios de pesagens
    driver.find_element(By.XPATH, '//div[@class="textdivresp"]').click()
    driver.find_elements(By.XPATH, '//div[@class="textdiv"]')[1].click()

    # selecionando o NOVO ASMOC como aterro
    aterro_field = driver.find_element(By.ID, 'paramATERRO2')
    aterro_field.send_keys('NOVO ASMOC')
    driver.find_element(By.XPATH, '//a[@id="paramDATAINI:dateButton"]').click()

    # selecionando a data da coleta dos dados (dia anterior)
    sleep(5)
    input_dataini =  driver.find_element(By.XPATH, '//input[@id="paramDATAINI"]')
    input_dataini.click()
    data_hoje = datetime.now()
    data_hoje_formatada = data_hoje.strftime("%d/%m/%Y")
    data_ontem = (data_hoje)-(timedelta(days=1))
    data_ontem_formatada = data_ontem.strftime("%d/%m/%Y")
    input_dataini.send_keys(data_ontem_formatada)
    input_datafinal =  driver.find_element(By.XPATH, '//input[@id="paramDATAFIM"]')
    input_datafinal.click()
    input_datafinal.send_keys(data_hoje_formatada)

    # clicando no botao de download da tabela
    driver.find_element(By.XPATH, '//button[@id="gerarExcel"]').click()
    sleep(15)
    log_final = log('Dados baixados com sucesso!')

    return log_final

@task(name='transform_coleta')
def transform_coleta() -> pd.DataFrame:
    '''
    Processa os dados para o formato do Data Warehouse
    
    Args: 
        None

    Returns: 
        df (pd.DataFrame) -> DataFrame dos dados baixados 
    '''
    
    # lendo o arquivo baixado
    for arquivo in os.listdir(PATH_DOWNLOAD):
        if arquivo.endswith('.xls'):
            nome_arquivo_xls = arquivo
            break

    df = pd.read_excel('./raw data/'+nome_arquivo_xls, header=2)

    # renomeando colunas para padrao do Data Warehouse
    df.columns = df.columns.str.lower()
    df.columns = df.columns.str.replace(' ','_')

    # criando colunas de fonte dos dados e descricao
    df['fonte'] = 'ACFOR'
    df['descricao'] = 'Dados extraídos do sistema ERP da Marquise'

    return df

@task (name='load_coleta')
def load_coleta(df:pd.DataFrame) -> str:
    '''
    Carregamento de dados no PostgreSQL (Data Warehouse)

    Args:
        df (pd.DataFrame) -> DataFrame dos dados

    Returns:
        None
    '''

    # realizando a conexao ao banco de dados do Observatorio (PostgreSQL)
    conn = psycopg2.connect(
        dbname=DBNAME,
        user=USERNAME_DB,
        password=PASSWORD_DB,
        host=HOST,
        port = PORT
    )

    # gerando dataset em formato csv
    data_ontem = (datetime.now())-(timedelta(days=1)).strftime("%d/%m/%Y")
    df.to_csv('br_acfor_relatorio_pesagem'+data_ontem+'.csv', index=False, encoding='utf-8')

    log_final = log('Carregamento efetuado com sucesso!')

    return log_final