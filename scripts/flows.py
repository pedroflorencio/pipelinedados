from prefect import flow
from tasks import extract_coleta, transform_coleta, load_coleta

@flow(name="etl_coleta_domiciliar", log_prints=True)
def etl_coleta_domiciliar():
    extract_coleta()
    df = transform_coleta()
    load_coleta(df)

if __name__ == "__main__":
    # deploy do flow
    etl_coleta_domiciliar.serve(name="my-first-deployment", 
                                cron='0 0 * * *')
